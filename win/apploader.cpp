#include "win/apploader.h"
#include <qdebug.h>

AppLoader::AppLoader(QObject *parent) : QObject(parent)
{
    this->initUtils();
    this->initBinariesNames();
    this->initPathAndFiles();
}

AppLoader::~AppLoader() {}

void AppLoader::initUtils()
{
    this->error_message = new QString;
    this->controller_result = new QMap<bool, QString>;
}

void AppLoader::initPathAndFiles()
{
    /* Packer execution Path */
    this->executionPath = QApplication::applicationFilePath();

    /* Server Folder */
    this->serverFolderPath = QDir::cleanPath(QDir::toNativeSeparators(this->executionPath + "/../server/"));
    this->serverFolderInfo =  new QFileInfo(this->serverFolderPath);

    /* App Folder */
    this->appFolderPath = QDir::cleanPath(QDir::toNativeSeparators(this->executionPath + "/../app/"));
    this->appFolderInfo = new QFileInfo(this->appFolderPath);

    /* Config File */
    this->configFilePath = QDir::cleanPath(QDir::toNativeSeparators(this->appFolderPath + "/app_packer.json"));
    this->configFileInfo = new QFileInfo(this->configFilePath);


    /* Node Binary */
    this->nodeBinPath = QDir::cleanPath(QDir::toNativeSeparators(this->serverFolderPath + "/"+this->nodeBin));
    this->nodeBinInfo =  new QFileInfo(this->nodeBinPath);

    /* NPM js cli */
    this->npmFilePath = QDir::cleanPath(QDir::toNativeSeparators(this->serverFolderPath + "/npm_folder/bin/npm-cli.js"));
    this->npmFileInfo =  new QFileInfo(this->npmFilePath);

    /* PHP Binary */
    this->phpBinPath = QDir::cleanPath(QDir::toNativeSeparators(this->serverFolderPath + "/php_server/"+this->phpBin));
    this->phpBinInfo =  new QFileInfo(this->phpBinPath);

    qDebug() << this->phpBinPath;

}

void AppLoader::initBinariesNames()
{
    qDebug() << "WINDOWS detected";
    this->phpBin += ".exe";
    this->nodeBin += ".exe";

}

QMap<bool, QString>* AppLoader::initApp()
{
    try {
        this->checkAppArchitecture();
        this->loadConfigFile();
        this->checkHostEnv(this->configJson.value("app_type").toString().toUtf8());

        this->controller_result->insert(true, "[GENERAL MSG] App initialized!");

        return this->controller_result;

    } catch (QString &str) {
        this->controller_result->insert(false, str);
        return this->controller_result;
    }
}

void AppLoader::checkHostEnv(QString host_type)
{
    // Handle Node.js type - No other as of 2015-08-10
    if (host_type.toLower() == "node") {

        /* Check if binary exists */
        if (!this->nodeBinInfo->exists() || !this->nodeBinInfo->isFile()) {
            throw QString("[GENERAL ERROR] No Node binary found in sources!");
        }
        if (!this->npmFileInfo->exists() || !this->npmFileInfo->isFile()) {
            throw QString("[GENERAL ERROR] No NPM js client found in sources!");
        }

        /* Check if binary is executable */
        QString command = "-v";
        QProcess *process = new QProcess(this);
        qDebug() << "[GENERAL MSG]  Command to be tested:" << this->nodeBinPath + " " + command;
        process->start(this->nodeBinPath, QStringList() << command);

        if (!process->waitForFinished()) {
            throw QString("[GENERAL ERROR] Node bin not found! Stop operations now.");
        }

    } else if (host_type.toLower() == "php") {
        /* Check if binary exists */
        if (!this->phpBinInfo->exists() || !this->phpBinInfo->isFile()) {
            throw QString("[GENERAL ERROR] No PHP binary found in sources at "+this->phpBinPath);
        }

        /* Check if binary is executable */
        QString command = "-v";
        QProcess *process = new QProcess(this);
        qDebug() << "[GENERAL MSG]  Command to be tested:" << this->phpBinPath + " " + command;
        process->start(this->phpBinPath, QStringList() << command);



        if (!process->waitForFinished()) {
            throw QString("[GENERAL ERROR] PHP bin not found! Stop operations now.");
        }
        qDebug() << process->readAllStandardError() << process->readAllStandardOutput();

    } else if (host_type.toLower() == "static") {
        /* Check if files exists */
        QString file = this->configJson.value("entry_point").toString().toUtf8();
        this->staticFileEntryPath = QDir::cleanPath(QDir::toNativeSeparators(this->appFolderPath + file));
        qDebug() << this->staticFileEntryPath;
        this->staticFileEntryInfo =  new QFileInfo(this->staticFileEntryPath);

        if (!this->staticFileEntryInfo->exists() || !this->staticFileEntryInfo->isFile()) {
            throw QString("[GENERAL ERROR] Specified static file is not a file or doesn't exist!");
        }
    } else {
        throw QString("[GENERAL ERROR] Unrecognized host_type value: \""+ host_type +"\"");
    }

}
QFileInfo *AppLoader::getStaticFileEntryInfo() const
{
    return this->staticFileEntryInfo;
}

void AppLoader::setStaticFileEntryInfo(QFileInfo *value)
{
    this->staticFileEntryInfo = value;
}

QString AppLoader::getStaticFileEntryPath() const
{
    return this->staticFileEntryPath;
}

void AppLoader::setStaticFileEntryPath(const QString &value)
{
    this->staticFileEntryPath = value;
}


QString AppLoader::getPhpBinPath() const
{
    return this->phpBinPath;
}

void AppLoader::checkAppArchitecture()
{
    if (!this->configFileInfo->exists() || !this->configFileInfo->isFile()) {
        throw QString("[GENERAL ERROR] Invalid config file or not found!");
    }

    if (!this->serverFolderInfo->exists() || !this->serverFolderInfo->isDir()) {
        throw QString("[GENERAL ERROR] No server folder in sources!");
    }

    if (!this->appFolderInfo->exists() || !this->appFolderInfo->isDir()) {
        throw QString("[GENERAL ERROR] No app folder in sources!");
    }
}

void AppLoader::loadConfigFile()
{
    this->configFile.setFileName(this->configFilePath);

    if (!this->configFile.open(QIODevice::ReadOnly | QIODevice::Text)) {;
        throw QString("[GENERAL ERROR] Failed to open config file json");
    }

    QString jsonData = QString::fromUtf8(this->configFile.readAll());
    this->configFile.close();
    QJsonDocument document = QJsonDocument::fromJson(jsonData.toUtf8());

    if (document.isNull()) {
        throw QString("[GENERAL ERROR] Empty or invalid json format in config file");
    }

    this->configJson = document.object();

    // Json values sample if quick test required
   /* qDebug() <<  this->configJson.value("app_type");
    qDebug() <<  this->configJson.value("host");
    qDebug() <<  this->configJson.value("port");
    qDebug() <<  this->configJson.value("entry_point");
    qDebug() <<  this->configJson.value("winHeight");
    qDebug() <<  this->configJson.value("winWidth");
    qDebug() <<  this->configJson.value("main_window_name"); */
}


/**
 *
 * AppLoader getter/setter
 *
 */

QString AppLoader::getAppFolderPath() const
{
    return this->appFolderPath;
}

void AppLoader::setAppFolderPath(const QString &value)
{
    this->appFolderPath = value;
}

QString AppLoader::getNpmFilePath() const
{
    return this->npmFilePath;
}

void AppLoader::setNpmFilePath(const QString &value)
{
    this->npmFilePath = value;
}

QString AppLoader::getNodeBinPath() const
{
    return this->nodeBinPath;
}

void AppLoader::setNodeBinPath(const QString &value)
{
    this->nodeBinPath = value;
}

QString AppLoader::getExecutionPath() const
{
    return this->executionPath;
}

void AppLoader::setExecutionPath(const QString &value)
{
    this->executionPath = value;
}

QString AppLoader::getServerFolderPath() const
{
    return this->serverFolderPath;
}

void AppLoader::setServerFolderPath(const QString &value)
{
    this->serverFolderPath = value;
}

QJsonObject AppLoader::getConfigJson() const
{
    return this->configJson;
}

void AppLoader::setConfigJson(const QJsonObject &value)
{
    this->configJson = value;
}

QString AppLoader::getConfigFilePath() const
{
    return this->configFilePath;
}

void AppLoader::setConfigFilePath(const QString &value)
{
    this->configFilePath = value;
}

QFileInfo *AppLoader::getConfigFileInfo() const
{
    return this->configFileInfo;
}

void AppLoader::setConfigFileInfo(QFileInfo *value)
{
    this->configFileInfo = value;
}

QFileInfo *AppLoader::getPhpBinInfo() const
{
    return this->phpBinInfo;
}

void AppLoader::setPhpBinInfo(QFileInfo *value)
{
    this->phpBinInfo = value;
}

