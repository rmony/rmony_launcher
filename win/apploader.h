#ifndef APPLOADER_H
#define APPLOADER_H

#include <QObject>
#include <QMap>
#include <QString>
#include <QApplication>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QProcess>
#include <QSysInfo>

class AppLoader : public QObject
{
    Q_OBJECT
public:
    explicit AppLoader(QObject *parent = 0);
    ~AppLoader();
    QMap<bool, QString>* initApp();
    QString *error_message;
    QMap<bool, QString> *controller_result;

    // Check if removal is required for them
    QFile getConfigFile() const;
    void setConfigFile(const QFile &value);
    QFileInfo *getConfigFileInfo() const;
    void setConfigFileInfo(QFileInfo *value);
    QString getConfigFilePath() const;
    void setConfigFilePath(const QString &value);
    // to be kept
    QJsonObject getConfigJson() const;
    void setConfigJson(const QJsonObject &value);
    QString getServerFolderPath() const;
    void setServerFolderPath(const QString &value);
    QString getExecutionPath() const;
    void setExecutionPath(const QString &value);
    QString getNodeBinPath() const;
    void setNodeBinPath(const QString &value);
    QString getNpmFilePath() const;
    void setNpmFilePath(const QString &value);
    QString getAppFolderPath() const;
    void setAppFolderPath(const QString &value);
    QFileInfo *getPhpBinInfo() const;
    void setPhpBinInfo(QFileInfo *value);
    QString getPhpBinPath() const;
    void setPhpBinPath(const QString &value);
    QString getStaticFileEntryPath() const;
    void setStaticFileEntryPath(const QString &value);
    QFileInfo *getStaticFileEntryInfo() const;
    void setStaticFileEntryInfo(QFileInfo *value);

private:
    void checkAppArchitecture();
    void loadConfigFile();
    void checkHostEnv(QString host_type);
    void initBinariesNames();
    void initPathAndFiles();
    void initUtils();

    QString executionPath;

    QString phpBin;
    QString phpBinPath;
    QFileInfo *phpBinInfo;

    QString nodeBin;
    QString nodeBinPath;
    QFileInfo *nodeBinInfo;

    QString staticFileEntryPath;
    QFileInfo *staticFileEntryInfo;

    QString npmFilePath;
    QFileInfo *npmFileInfo;

    QString serverFolderPath;
    QFileInfo *serverFolderInfo;

    QString appFolderPath;
    QFileInfo *appFolderInfo;


    QString configFilePath;
    QFileInfo *configFileInfo;
    QFile configFile;
    QJsonObject configJson;


signals:

public slots:
};

#endif // APPLOADER_H
