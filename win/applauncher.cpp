#include "win/applauncher.h"
#include <qdebug.h>
#include <QStringList>
#include <QProcessEnvironment>

AppLauncher::AppLauncher(AppLoader *_appLoader, WebView *_webView)
{
    this->appLoader = _appLoader;
    this->webView = _webView;
    this->controller_result = new QMap<bool, QString>;
    QPixmap pixmap(":/splash/splash.jpg");
    this->splashLoader = new QSplashScreen(pixmap);
}

AppLauncher::~AppLauncher()
{

}

void AppLauncher::applySettings()
{
    this->applyWindowSizeSettings();
    this->applyWindowNameSettings();
    this->applyStartUrlSettings();
}


void AppLauncher::applyWindowNameSettings()
{
    // Check for main window name
    if (!this->appLoader->getConfigJson().value("main_window_name").toString().isNull()) {
        this->webView->setWindowTitle(this->appLoader->getConfigJson().value("main_window_name").toString().toUtf8());
    } else {
        QString default_title = "A Packaged App made with \u2665";
        this->webView->setWindowTitle(default_title.toUtf8());
    }
}

void AppLauncher::applyStartUrlSettings()
{
    // Set home based on user input
    QString app_type = this->appLoader->getConfigJson().value("app_type").toString();
    if (app_type == "static") {
        QString protocol = "file://";
        QString entry_point = this->appLoader->getConfigJson().value("entry_point").toString();
        QString home_path = protocol+this->appLoader->getAppFolderPath()+entry_point;
        qDebug() << "home path" << home_path;
        this->webView->setWebViewURL(home_path);

    } else {
        this->webView->setWebViewURL(
                                        this->appLoader->getConfigJson().value("host").toString(),
                                        this->appLoader->getConfigJson().value("port").toString()
                                    );
    }
}

void AppLauncher::applyWindowSizeSettings()
{
    // Window Size or default, probably to be recoded :/
    if (!this->appLoader->getConfigJson().value("winHeight").toString().isNull() &&
        !this->appLoader->getConfigJson().value("winWidth").toString().isNull()) {

        this->webView->setWindowSize(
                                        atoi(this->appLoader->getConfigJson().value("winHeight").toString().toUtf8()),
                                        atoi(this->appLoader->getConfigJson().value("winWidth").toString().toUtf8())
                                    );
    } else {
        QDesktopWidget wid;
        int defaultWidth = wid.screen()->width() / 2;
        int defaultHeight = wid.screen()->height() / 2;
        this->webView->setWindowSize(
                                        defaultHeight,
                                        defaultWidth
                                    );
    }
}

QMap<bool, QString>* AppLauncher::startApplication()
{
    try {
        this->splashLoader->show();
        // Launch Server
        this->startServer();
        // Apply user Config
        this->applySettings();
        // Nothing catch so far .. go ahead
        this->controller_result->insert(true, "[GENERAL MSG] Server and app started");
        this->splashLoader->hide();
        return this->controller_result;

    } catch (QString &str) {
        this->splashLoader->hide();
        this->controller_result->insert(false, str);
        return this->controller_result;
    }
}

void AppLauncher::startServer()
{
    QString app_type = this->appLoader->getConfigJson().value("app_type").toString().toLower();
    if (this->isFirstStart()) {
        this->splashLoader->showMessage("Init first start", Qt::AlignCenter, Qt::white);
        if (app_type == "node") {
            this->execNpmUpdate();
        } else if (app_type == "php") {
            this->execComposerInstall();
        }
    }
    this->splashLoader->showMessage("Start server", Qt::AlignCenter, Qt::white);
    if (app_type == "node") {
        this->execNodeServer();
    } else if (app_type == "php") {
        this->execPhpServer();
    }
}

bool AppLauncher::isFirstStart()
{
    QString archMonyInitPath = QDir::cleanPath(QDir::toNativeSeparators(this->appLoader->getExecutionPath() + "/../archmony.init"));
    QFileInfo *archMonyInitFileInfo = new QFileInfo(archMonyInitPath);

    if (archMonyInitFileInfo->exists() && archMonyInitFileInfo->isFile()) {
        return false;
    }
    // Create file if it wasnt already existing
    QFile *archMonyInitFile = new QFile(archMonyInitPath);
    archMonyInitFile->open(QIODevice::WriteOnly);
    return true;
}

void AppLauncher::execNodeServer()
{
    qDebug() << "node server launch";
    QString command = this->appLoader->getAppFolderPath() + "/" + this->appLoader->getConfigJson().value("entry_point").toString();
    QProcess *process = new QProcess(this->webView);
    process->start(this->appLoader->getNodeBinPath(), QStringList() << command);
    process->waitForStarted();
    process->waitForReadyRead();
    process->waitForBytesWritten();

    /* When node server is launched it should never finish until program quit */
    /*if (process->waitForFinished(5000)) {
        throw QString("[GENERAL ERROR] Unable to launch Node.js binary");
    }*/

    // Give the node server to webView
    this->webView->setServerProcess(process);
}

void AppLauncher::execPhpServer()
{
    QString host = this->appLoader->getConfigJson().value("host").toString();
    QString port = this->appLoader->getConfigJson().value("port").toString();
    QString entry_point = this->appLoader->getConfigJson().value("entry_point").toString();

    QString prog;
    QString shell_args;

    QProcess* process = new QProcess(this->webView);

    #ifdef Q_OS_WIN
        prog = "cmd.exe";
        shell_args = "";
        QString arguments = shell_args + this->appLoader->getPhpBinPath() + " -S "+host+":"+port+ " -t "+QDir(this->appLoader->getAppFolderPath()+entry_point).path();
        qDebug() << "prog" << prog << "args" << arguments;

        process->start(arguments);
    #endif
    #ifdef Q_OS_OSX
        prog = "/bin/bash";
        shell_args = "-c";
        QStringList arguments;
        arguments << shell_args << this->appLoader->getPhpBinPath() + " -S "+host+":"+port+ " -t "+this->appLoader->getAppFolderPath()+entry_point;
        process->start(prog , arguments);
    #endif

    process->waitForStarted();

    // Give th PHP server to webView
    this->webView->setServerProcess(process);
}

void AppLauncher::execComposerInstall()
{
    // TODO
}

void AppLauncher::execNpmUpdate()
{
    this->splashLoader->showMessage("npm update", Qt::AlignCenter, Qt::white);
    qDebug() << "npm update launch";
    QString command = this->appLoader->getNpmFilePath();
    QString workingDir = QDir::cleanPath(QDir::toNativeSeparators(this->appLoader->getAppFolderPath() + "/"));
    QProcess *process = new QProcess(this->webView);
    process->setWorkingDirectory(workingDir);
    process->start(this->appLoader->getNodeBinPath(), QStringList() << command << "install");
    qDebug() << process->program() << process->arguments();
    qDebug() << this->appLoader->getNodeBinPath() << command << workingDir;

    process->waitForStarted();
    process->waitForReadyRead();
    process->waitForBytesWritten();
    if (process->waitForFinished(120000)) {
        throw QString("[GENERAL ERROR] Unable to execute NPM INSTALL");
    }
}

