#ifndef ERRORVIEW_H
#define ERRORVIEW_H

#include <QDialog>

namespace Ui {
class ErrorView;
}

class ErrorView : public QDialog
{
    Q_OBJECT

public:
    explicit ErrorView(QWidget *parent = 0);
    ~ErrorView();

    QString getErrorMsg() const;
    void setErrorMsg(const QString &value);

private slots:
    void on_ackQuitApp_clicked();

private:
    Ui::ErrorView *ui;
    QString errorMsg;
};

#endif // ERRORVIEW_H
