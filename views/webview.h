#ifndef WEBVIEW_H
#define WEBVIEW_H

#include <QMainWindow>
#include <QProcess>
#include <QDesktopWidget>
#ifdef Q_OS_WIN
    #include "win/apploader.h"
#endif
#ifdef Q_OS_OSX
    #include "osx/apploader.h"
#endif

namespace Ui {
    class WebView;
}

class WebView : public QMainWindow
{
    Q_OBJECT

public:
    explicit WebView(QWidget *parent = 0);
    ~WebView();
    Ui::WebView *getUi() const;
    void setUi(Ui::WebView *value);
    void setWebViewURL(QString host, QString port = NULL);
    QProcess *getServerProcess() const;
    void setServerProcess(QProcess *value);

    void setWindowSize(int winHeight, int winWidth);
    AppLoader *getAppLoader() const;
    void setAppLoader(AppLoader *value);

public slots:
    void closeEvent(QCloseEvent *event);

private:
    Ui::WebView *ui;
    QProcess *serverProcess;
    AppLoader *appLoader;
};

#endif // WEBVIEW_H
