#include "views/webview.h"
#include "ui_webview.h"
#include "qdebug.h"

WebView::WebView(QWidget *parent) : QMainWindow(parent), ui(new Ui::WebView)
{
    this->ui->setupUi(this);
    // Enable developper mode if required. ATM always true waiting for debug flags to be implemented
    QWebSettings::globalSettings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);
}

WebView::~WebView()
{
    delete this->ui;
    this->serverProcess->terminate();
}
Ui::WebView *WebView::getUi() const
{
    return this->ui;
}

void WebView::setUi(Ui::WebView *value)
{
    this->ui = value;
}

void WebView::setWebViewURL(QString host, QString port)
{
    if (port.isNull()) {
        this->ui->mainWebView->setUrl(host);
         qDebug() << "URL SET TO:" << host;
    } else {
        this->ui->mainWebView->setUrl("http://" + host + ":" + port);
         qDebug() << "URL SET TO:" << "http://" + host + ":" + port;
    }

}

void WebView::setWindowSize(int winHeight, int winWidth)
{
    QDesktopWidget wid;
    int screenWidth = wid.screen()->width();
    int screenHeight = wid.screen()->height();

    this->setGeometry((screenWidth/2)-(this->frameGeometry().width()/2),
                     (screenHeight/2)-(this->frameGeometry().height()/2),
                     winWidth, winHeight);
}

QProcess *WebView::getServerProcess() const
{
    return this->serverProcess;
}

void WebView::setServerProcess(QProcess *value)
{
    this->serverProcess = value;
}


void WebView::closeEvent(QCloseEvent *event)
{
    if (this->appLoader->getConfigJson().value("app_type").toString().toLower() != "static") {
        this->serverProcess->terminate();
    }
}
AppLoader *WebView::getAppLoader() const
{
    return this->appLoader;
}

void WebView::setAppLoader(AppLoader *value)
{
    this->appLoader = value;
}


