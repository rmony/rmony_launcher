#include "views/errorview.h"
#include "ui_errorview.h"

ErrorView::ErrorView(QWidget *parent) : QDialog(parent), ui(new Ui::ErrorView)
{
    this->ui->setupUi(this);
}

ErrorView::~ErrorView()
{
    delete this->ui;
}

void ErrorView::on_ackQuitApp_clicked()
{
    QApplication::quit();
}

QString ErrorView::getErrorMsg() const
{
    return this->ui->ackText->text();
}

void ErrorView::setErrorMsg(const QString &value)
{
    this->ui->ackText->setText(value);
}

