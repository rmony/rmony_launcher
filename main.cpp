//Views
#include "views/webview.h"
#include "views/errorview.h"

// Controllers
#ifdef Q_OS_WIN
    #include "win/applauncher.h"
    #include "win/apploader.h"
#endif
#ifdef Q_OS_OSX
    #include "osx/applauncher.h"
    #include "osx/apploader.h"
#endif

// QT
#include <QApplication>
#include <QFileInfo>
#include <QCoreApplication>
#include <QMessageBox>

//Debug
#include <qdebug.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    /* Views */
    WebView *webView = new WebView();
    ErrorView *errorView = new ErrorView();
    /* Controllers */
    AppLoader *appLoader = new AppLoader();
    webView->setAppLoader(appLoader);
    AppLauncher *appLauncher = new AppLauncher(appLoader, webView);

    QMap<bool, QString> init_response = appLoader->initApp()[0];

    /* Start the steam machiiiine ! */
    if (init_response.firstKey() == true) {
       QMap<bool, QString> run_response = appLauncher->startApplication()[0];
       qDebug() << run_response;
       if (run_response.firstKey() == true) {
            webView->show();
       } else {
            errorView->setErrorMsg(run_response.first());
            errorView->show();
       }
    } else {
       errorView->setErrorMsg(init_response.first());
       errorView->show();
    }

    return a.exec();
}
