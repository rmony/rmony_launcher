#-------------------------------------------------
#
# Project created by tchauviere 2015-08-07T23:48:51
#
#-------------------------------------------------

QT       += core gui webkit webkitwidgets webchannel

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = rMony
TEMPLATE = app

#
#   COMMON HEADERS AND SOURCES
#
SOURCES +=  main.cpp \
            views/webview.cpp \
            views/errorview.cpp \
            interfaces/iapploader.cpp

HEADERS  += views/webview.h \
            views/errorview.h \
            interfaces/iapploader.h

#
#   PLATFORM SPECIFICS HEADERS AND SOURCES
#
win32 {
    SOURCES +=  win/apploader.cpp \
                win/applauncher.cpp

    HEADERS  += win/apploader.h \
                win/applauncher.h
}

macx {
    # Change this depending on which version you have from XCode in:
    # /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs
    QMAKE_MAC_SDK = macosx10.11

    SOURCES +=  osx/apploader.cpp \
                osx/applauncher.cpp

    HEADERS +=  osx/apploader.h \
                osx/applauncher.h \
}

FORMS    += views/webview.ui \
            views/errorview.ui

DISTFILES += \
            .gitignore

RESOURCES += \
            rsrc.qrc
