#ifndef IAPPLOADER_H
#define IAPPLOADER_H

#include <QObject>
#include <QMap>
#include <QString>
#include <QApplication>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QProcess>
#include <QSysInfo>

#include <QDebug>
#include "qdebug.h"

class IAppLoader: public QObject
{
    Q_OBJECT

    public:
        /* ctor / dtor */
         explicit IAppLoader(QObject *parent = 0);
        ~IAppLoader();

        /* Controller members used to get actions results from outside itself */
        QString *error_message;
        QMap<bool, QString> *controller_result;

        /* Controller generalistics methods */
        QMap<bool, QString>* initApp();

        /* Getters / Setters */
        QFile getConfigFile() const;
        void setConfigFile(const QFile &value);
        QFileInfo *getConfigFileInfo() const;
        void setConfigFileInfo(QFileInfo *value);
        QString getConfigFilePath() const;
        void setConfigFilePath(const QString &value);
        QJsonObject getConfigJson() const;
        void setConfigJson(const QJsonObject &value);
        QString getServerFolderPath() const;
        void setServerFolderPath(const QString &value);
        QString getExecutionPath() const;
        void setExecutionPath(const QString &value);
        QString getNodeBinPath() const;
        void setNodeBinPath(const QString &value);
        QString getNpmFilePath() const;
        void setNpmFilePath(const QString &value);
        QString getAppFolderPath() const;
        void setAppFolderPath(const QString &value);
        QFileInfo *getPhpBinInfo() const;
        void setPhpBinInfo(QFileInfo *value);
        QString getPhpBinPath() const;
        void setPhpBinPath(const QString &value);
        QString getStaticFileEntryPath() const;
        void setStaticFileEntryPath(const QString &value);
        QFileInfo *getStaticFileEntryInfo() const;
        void setStaticFileEntryInfo(QFileInfo *value);

    protected:
         /* Protected controller method members */
        void checkAppArchitecture();
        void loadConfigFile();
        void initPathAndFiles();
        void initUtils();
        void checkHostEnv(QString host_type);

        QString phpBin;
        QString nodeBin;

    private:
         /* Private controller method members */
        virtual void initBinariesNames() = 0;

        /* Private controller variable members */
        QString executionPath;

        QString phpBinPath;
        QFileInfo *phpBinInfo;

        QString nodeBinPath;
        QFileInfo *nodeBinInfo;

        QString staticFileEntryPath;
        QFileInfo *staticFileEntryInfo;

        QString npmFilePath;
        QFileInfo *npmFileInfo;

        QString serverFolderPath;
        QFileInfo *serverFolderInfo;

        QString appFolderPath;
        QFileInfo *appFolderInfo;

        QString configFilePath;
        QFileInfo *configFileInfo;
        QFile configFile;
        QJsonObject configJson;
};

#endif // IAPPLOADER_H
