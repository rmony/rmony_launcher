#include "osx/apploader.h"

/* ctor / dtor */
AppLoader::AppLoader() {}
AppLoader::~AppLoader() {}

/* OSX Specific methods */
void AppLoader::initBinariesNames()
{
    qDebug() << "OSX detected";
    this->phpBin = "php";
    this->nodeBin = "node";
}
