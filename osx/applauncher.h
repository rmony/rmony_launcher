#ifndef APPLAUNCHER_H
#define APPLAUNCHER_H

#include <QObject>
#include <QProcess>
#include <QDir>
#include <QSplashScreen>
#include <QSysInfo>
#include "osx/apploader.h"
#include <views/webview.h>

class AppLauncher : public QObject
{
    Q_OBJECT
public:
    AppLauncher(AppLoader *_appLoader, WebView *_webView);
    ~AppLauncher();
     QMap<bool, QString> * startApplication();
     QSplashScreen *splashLoader;

private:
    AppLoader *appLoader;
    WebView *webView;
    QMap<bool, QString> *controller_result;
    void startServer();
    void applySettings();
    void applyWindowSizeSettings();
    void applyWindowNameSettings();
    void execNpmUpdate();
    void execNodeServer();
    void execPhpServer();
    void execComposerInstall();
    bool isFirstStart();
    void applyStartUrlSettings();
    void execStaticServer();
};

#endif // APPLAUNCHER_H
