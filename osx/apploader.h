#ifndef APPLOADER_H
#define APPLOADER_H

#include "interfaces/iapploader.h"

class AppLoader : public IAppLoader
{

    public:
         AppLoader();
        ~AppLoader();

    private:
        void initBinariesNames();

};

#endif // APPLOADER_H
